<?php

// required headers for handling JSON POST data //
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get posted data
$data = json_decode(file_get_contents("php://input"));

class Sales{

	// Member variables to capture & maintain the data //
	public $productList;
	public $productPrice;
	public $productTierPrice;
	public $grandTotal;
	public $productFinalPrice;
	
	public function __construct()
	{
		$this->productList = array();
		$this->productFinalPrice = array();
		$this->grandTotal = 0;
	}
	
	/**
	* Method to configure Product Price & Tier Price
	* @Param $productDetails array ( product code, qty, price )
	* @return $productPrice, $productTierPrice 
	**/
	public function setPricing($productDetails = array())
	{
		try{
			if(count($productDetails)){
				foreach($productDetails as $productCode => $productData)
				{
					if($productData['qty'] == 1)
					{
						$this->productPrice[$productData['sku']] = $productData['price'];
					}else
					{
						$this->productTierPrice[$productData['sku']] = $productData;
					}
				}
			}
		}catch(Exception $e)
		{
			// Response with error message
			echo json_encode( array("error_message" =>  $e->getMessage()));
			die;
		}
	}
	
	/**
	*
	* Method to capture items one by one
	* @param $product ( Product Code )
	*
	**/
	public function Scan($product)
	{
		if($product)
		{
			if(array_key_exists($product,$this->productList))
				$this->productList[$product] = $this->productList[$product]+1;
			else
				$this->productList[$product] = 1;
			
			// Call calculateTotalPrice() function to return the overall cart items on every scan //
			$this->calculateTotalPrice($product);
		}else{
			// Response with error message
			echo json_encode( array("error_message" =>  'Product Code is missing'));
			die;
		}
	}
	
	/**
	* Method to calculate Grand Total Amount
	* @param $productSku
	* @return $grandTotal
	**/
	public function calculateTotalPrice($productSku)
	{
		try{
			// Check the product qty against configured tier price qty //
			
			if(isset($this->productTierPrice[$productSku]) && $this->productTierPrice[$productSku]['qty'] <= $this->productList[$productSku] )
			{
				// Get tier price if product total qty is equal tier price qty //
				if($this->productTierPrice[$productSku]['qty'] == $this->productList[$productSku]){
					$this->productFinalPrice[$productSku] = $this->productTierPrice[$productSku]['price'];
				}else{ // Calculate total price ( including tier price and single qty price also //
					$tierQtyCount = (int) ($this->productList[$productSku] / $this->productTierPrice[$productSku]['qty']) ;
					$remainingQty = $this->productList[$productSku] % $this->productTierPrice[$productSku]['qty'];
					$this->productFinalPrice[$productSku] = ( $tierQtyCount * $this->productTierPrice[$productSku]['price'] ) + ( $remainingQty * $this->productPrice[$productSku] );
				}
			}else{ // product qty < tier price qty //
				if(isset($this->productPrice[$productSku])){
					$this->productFinalPrice[$productSku] = $this->productPrice[$productSku] * $this->productList[$productSku];
				}else{
					echo json_encode( array("error_message" =>  'Product price is not configured for the item '.$productSku));
					die;
				}
			}
			// Sum overall product price //
			$this->grandTotal = array_sum($this->productFinalPrice);
		}catch(Exception $e){
			// return error message
			echo json_encode( array("error_message" =>  $e->getMessage()));
			die;
		}
	}
}

// Check post data is not empty //
if(isset($data))
{
	// Create object for main sales class //
	$scan = new Sales();
	
	/**
	* Assigning Price & Tier Price
	* It can be configured in DB level also.
	**/
	$productData = array(
					array('sku'=>'A','qty'=>1,'price'=>2.00),
					array('sku'=>'A','qty'=>4,'price'=>7.00),
					array('sku'=>'B','qty'=>1,'price'=>12.00),
					array('sku'=>'C','qty'=>1,'price'=>1.25),
					array('sku'=>'C','qty'=>6,'price'=>6.00),
					array('sku'=>'D','qty'=>1,'price'=>0.15)
					);
	$scan->setPricing($productData);

	// Exploding the captured post data //	
	$data->products = explode(",",$data->products);
	
	foreach($data->products as $data){
		$scan->Scan($data);
	}
	// Return Grand Total Amount //
	echo json_encode(array('total'=> $scan->grandTotal));
	
}else{
	// Return Error Code & Message //
	echo json_encode(array('error_code'=>'100', 'error_message'=>'Product Code(s) are missing'));
}

/********************* Sample Input Data *******************************

URL : http://localhost/PointOfSaleApi.php
Input Type : JSON
Input Data : Products Code as comma separated value
Ex : 
{
'products':'A,B,C,D,C,C,C'
}

************************************************************************/

?>