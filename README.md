## Synopsis

This PHP file contains a logic of supporting point of sale system calculation. 

## Motivation

This is one of my collection of examples to demonstrate how the point of sale system price calculation will be happened.  The intent of this sample is to demonstrate how on the fly point of sale calculation is happening and return JSON based response.

## Technical feature

A Simple PHP class file created to complete the price calculation logic for list of catalog.


## Installation

Step1 : Please copy & paste the file on your xampp / wamp htodcs dicrectory

Step2 : Open one of the web service testing tool like Postman desktop app / some other tool which is supporting API testing

Step3 : trigger the below url with param in Postman app

http://localhost/PointOfSaleApi.php

Body - Raw

{
"products":"A,B,C,D"
}

The Result is,

{
"total": 15.4
}
## Contributors

Kirukan Core team

## License

[Open Source License](LICENSE.txt)